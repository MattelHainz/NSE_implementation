import socket
import struct
import asyncio

import hexdump

from core.config import config


class Gossip:

    reader = asyncio.StreamReader
    writer = asyncio.StreamWriter

    async def connect(self):

        try:
            self.reader, self.writer = await asyncio.open_connection('127.0.0.1', 7001)
        except:
            print("[+] Connecting to Gossip failed!")

        print("[+] Connected to gossip module:", ('127.0.0.1', 7001))

        self.send_notify()

        print("[+] Waiting for GOSSIP_NOTIFICATION message...")
        while True:
            await self.waiting_for_notification()


    def send_notify(self):
        # prepare packet payload
        bsize = 4 + 4
        buf = struct.pack(">HHHH", bsize, int(config['MESSAGE_TYPE']['GOSSIP_NOTIFY']), 0, 5220)

        # Send payload
        # self.connection.send(buf)
        self.writer.write(buf)
        print(f"[+] Sent GOSSIP_NOTIFY for type {'NSE_FLOOD'}.")

    def send_announce(self, message):
        # prepare packet payload
        bsize = 4 + 4 + len(message)
        buf = struct.pack(">HHBBH", bsize, int(config['MESSAGE_TYPE']['GOSSIP_ANNOUNCE']), 4, 0, 5220)
        buf += bytes(message.encode('UTF-8'))

        #print(f"[+] Prepared GOSSIP_ANNOUNCE packet:")
        #hexdump.hexdump(buf)

        # Send payload
        self.writer.write(buf)
        print(f"[+] Sent GOSSIP_ANNOUNCE ({5220}, {message})")

    async def waiting_for_notification(self):
        msizebuf = await self.reader.read(2)
        buflen = struct.unpack(">H", msizebuf)[0]
        buf = msizebuf + await self.reader.read(buflen)

        msize, mtype, mid, dtype = struct.unpack(">HHHH", buf[:8])
        mdata = buf[8:]

        if mtype != int(config['MESSAGE_TYPE']['GOSSIP_NOTIFICATION']):
            reason = f"Wrong packet type: {mtype}"
            print(reason)

        print(f"[+] Got GOSSIP_NOTIFICATION: mID = {mid}, type = {dtype}, "
              + f"data = {mdata}")

        hexdump.hexdump(buf)
        return mid
