import struct
import core.estimate_protocol as estimate_protocol
from core.config import config

from core.client_handler import bad_packet

message_type = config['MESSAGE_TYPE']


async def message_handler(buf, reader, writer):
    ret = False
    header = buf[:4]

    mtype = struct.unpack(">HH", header)[1]
    if mtype == int(message_type['NSE_QUERY']):
        ret = await handle_nse_query(buf, reader, writer)
    else:
        await bad_packet(reader, writer,
                         f"Unknown message type {mtype} received",
                         header)
    return ret


async def handle_nse_query(buf, reader, writer):
    raddr, rport = writer.get_extra_info('socket').getpeername()
    print(f"[+] {raddr}:{rport} >>> NSE_QUERY")

    msize = 12

    # TODO : send the real peer estimate and deviation using the GNU NSE protocol.
    NSE_PEER_ESTIMATE = int(estimate_protocol.peer_count)
    NSE_DEVIATION = int(estimate_protocol.standard_deviation)

    # send NSE Estimate message
    buf = struct.pack(">HHII", msize, int(message_type['NSE_ESTIMATE']),
                      NSE_PEER_ESTIMATE,
                      NSE_DEVIATION)

    try:
        writer.write(buf)
        await writer.drain()
    except Exception as e:
        print(f"[-] Failed to send NSE_ESTIMATE: {e}")
        await bad_packet(reader, writer)
        return False

    print(f"[+] {raddr}:{rport} <<< NSE_ESTIMATE({NSE_PEER_ESTIMATE}, "
          + f"{NSE_DEVIATION})")

    return True


async def send_gossip_announce(reader, writer, data):
    # TODO : send NSE_FLOOD and it's data via gossip announce

    return True
