import math
import asyncio
import json
import struct
import time
from random import randint
from core.gossip import Gossip
from hashlib import sha256
from core.config import config

peer_count = 1
standard_deviation = 0.0


class EstimateProtocol:
    # x / p
    proximity = 0
    # p'
    previous_proximity = 0

    # to calculate p~ (average p values)
    proximity_total = 0
    local_iteration = 1

    # S
    round_time = 0

    async def estimate_peer_count(self):
        loop = asyncio.get_event_loop()
        gossip = Gossip()
        # connect to gossip module and call gossip notify to subscribe into NSE_FLOOD message.
        loop.create_task(gossip.connect())

        # TODO : create public key for the node.
        # random public key for now
        public_key = str(randint(0, 100000))
        print(f"[+] My ID = {public_key}")

        while True:
            # calculate the waiting time until next calculate peer protocol fired
            frequency = int(config['NSE']['FREQUENCY'])

            # time.time() is current UTC time since epoch
            current_time = time.time()
            seconds_until_next_round = frequency - (current_time % frequency)

            print(f"[+] Waiting for next round = {seconds_until_next_round} seconds")

            await asyncio.sleep(seconds_until_next_round)

            self.round_time = current_time + seconds_until_next_round
            print(f"[+] Time to calculate peer count estimation! ({self.round_time})")

            # TODO : real node id, hash public key of the node.
            node_id = sha256(public_key.encode('utf-8'))
            t = sha256(str(self.round_time).encode('utf-8'))

            # calculate p
            self.calculate_proximity(t, node_id)

            flood_message = self.pack_flood_message()

            # Delay to flood the network
            time_to_flood = self.get_time_to_flood(self.proximity)

            delay = time_to_flood - self.round_time
            print(f"[+] Delay flooding the network for {delay} seconds")
            await asyncio.sleep(delay)

            # Flood network
            gossip.send_announce(flood_message)

            # Calculate peer count for NSE client
            self.calculate_peer_count()

            # Set parameter for next round
            self.previous_proximity = self.proximity

            self.local_iteration += 1

    def pack_flood_message(self):
        # send the flood message as JSON

        hop_count = 0
        public_key = 0
        proof_of_work = 0
        signature = 0

        message = {
            "hop_count": hop_count,
            "round_time": self.round_time,
            "proximity": self.proximity,
            "public_key": public_key,
            "proof_of_work": proof_of_work,
            "signature": signature
        }

        return json.dumps(message)

    def calculate_peer_count(self):
        global peer_count
        global standard_deviation

        # p~
        proximity_average = self.proximity_total / self.local_iteration
        peer_count = math.ceil(pow(2, (proximity_average - 0.332747)))
        print(f"[+] Peer Counted =  {peer_count}")

    def calculate_proximity(self, target_key, node_id):
        # XOR both bits and check how many leading 0 (0 in XOR means it is same bit)
        xor = hex(int(target_key.hexdigest(), 16) ^ int(node_id.hexdigest(), 16))
        xor_bits = "{0:08b}".format(int(xor, 16))

        self.proximity = len(xor_bits) - len(xor_bits.strip('0'))
        print(f"[+] Proximity (Overlapping Bits) =  {self.proximity}")

        self.proximity_total += self.proximity

    # r(x)
    def get_time_to_flood(self, x):
        frequency = int(config['NSE']['FREQUENCY'])
        time_to_flood = self.round_time + (frequency / 2) - (
                (frequency / math.pi) * math.atan(x - self.previous_proximity))

        return time_to_flood
