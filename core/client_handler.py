import struct
import hexdump


async def client_handler(reader, writer, handle_message, cleanup_func=None):
    client_address, client_port = writer.get_extra_info('socket').getpeername()
    print(f"[+] {client_address}:{client_port} >>> Incoming connection")

    while True:
        buf = await read_message(reader, writer, cleanup_func=cleanup_func)
        if buf == b'':
            return

        if not await handle_message(buf, reader, writer):
            return


async def read_message(reader, writer, cleanup_func=None):
    if writer.is_closing():
        await bad_packet(reader, writer, cleanup_func=cleanup_func)
        return b''

    try:
        client_address, client_port = writer.get_extra_info('socket').getpeername()
    except OSError:
        await bad_packet(reader, writer, cleanup_func=cleanup_func)
        return b''

    try:
        msizebuf = await reader.read(2)
        buflen = struct.unpack(">H", msizebuf)[0]
        buf = msizebuf + await reader.read(buflen)
    except Exception as e:
        if msizebuf != b'':
            await bad_packet(reader, writer,
                             "Malformed data received", msizebuf, cleanup_func)
        else:
            await bad_packet(reader, writer, cleanup_func=cleanup_func)

        buf = b''
    return buf


async def bad_packet(reader, writer, reason='', data=b'', cleanup_func=None):
    try:
        client_address, client_port = writer.get_extra_info('socket').getpeername()
    except OSError:
        writer.close()
        await writer.wait_closed()
        print(f"[i] (Unknown) xxx Connection closed abruptly, state may remain")
        return

    if reason != '':
        print(f"[-] {client_address}:{client_port} >>> {reason}:\n")
        hexdump.hexdump(data)
        print('')

    if cleanup_func:
        await cleanup_func((reader, writer))

    writer.close()
    await writer.wait_closed()
    print(f"[i] {client_address}:{client_port} xxx Connection closed")
