import argparse
import asyncio
import socket
import struct

from core.config import config
from core.estimate_protocol import EstimateProtocol
from core.client_handler import client_handler
from core.message_handler import message_handler


def main():
    nse_port = config['NSE']['PORT']

    # parse commandline arguments to manually port, otherwise use value from config.ini
    usage_string = ("Run NSE module server.")
    cmd = argparse.ArgumentParser(description=usage_string)
    cmd.add_argument("-p", "--port",
                     help="Bind server to this port")
    args = cmd.parse_args()

    if args.port is not None:
        nse_port = args.port

    loop = asyncio.get_event_loop()

    # Create new estimate in defined frequency in config
    loop.create_task(EstimateProtocol().estimate_peer_count())

    # create asyncio server to listen for incoming API messages
    handler = lambda r, w, mhandler=message_handler: client_handler(r,
                                                                  w,
                                                                  mhandler)
    serv = asyncio.start_server(handler,
                                host='127.0.0.1', port=nse_port,
                                family=socket.AddressFamily.AF_INET,
                                reuse_address=True,
                                reuse_port=True)
    loop.create_task(serv)
    print(f"[+] NSE listening on {'127.0.0.1'}:{nse_port}")

    try:
        loop.run_forever()
    except KeyboardInterrupt as e:
        print("[i] Received SIGINT, shutting down...")
        loop.stop()


if __name__ == '__main__':
    main()
